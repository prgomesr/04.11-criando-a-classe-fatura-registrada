package br.com.ozeano.curso.api.bb.infra.model.input;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@JsonInclude(Include.NON_NULL)
public class DescontoInput {

	private Integer tipo;
	private String dataExpiracao;
	private Long porcentagem;
	private BigDecimal valor;
	
}
