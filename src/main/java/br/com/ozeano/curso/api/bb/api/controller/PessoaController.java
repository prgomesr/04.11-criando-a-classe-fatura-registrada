package br.com.ozeano.curso.api.bb.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ozeano.curso.api.bb.domain.model.Pessoa;
import br.com.ozeano.curso.api.bb.domain.repository.PessoaRepository;

@RequestMapping("pessoas")
@RestController
public class PessoaController {

	@Autowired
	private PessoaRepository repository;
	
	@GetMapping
	public List<Pessoa> listar() {
		return repository.findAll();
	}
	
}
