package br.com.ozeano.curso.api.bb.domain.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Conta extends BaseEntity {

	private String agencia;
	private String conta;
	private String digitoAgencia;
	private String digitoConta;
	
	@ManyToOne
	@JoinColumn(name = "banco_id")
	private Banco banco;
	
	@ManyToOne
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
}
